# PD2AF Converter

PD2AF is a tool intended for translation of SBGN maps from Process Description to Activity Flow language.

## Project Navigation

* [REPOSITORY](https://gitlab.com/pd2af/pd2af) with all the relevant project's data
* [WEB INTERFACE](http://188.166.159.222/translator.html) for converter
* [SBGN EXAMPLES](https://gitlab.com/pd2af/pd2af/tree/master/knowledge/sbgn_examples/metabolismregulation.org) from *Metabolism Regulation project*
* [ISSUES](https://gitlab.com/pd2af/pd2af/issues) with current bugs and planned features
* [CONVERTER RULES](https://gitlab.com/ddenniss/odysseus_modules/blob/master/pd2af/pd2af.rkt) implemented in Racket language
* [TRANSLATION ONTOLOGY](https://gitlab.com/pd2af/pd2af/tree/master/knowledge/sbgn_translation_ontology/sbgn_translation.tree) that describes terms and ideas, used in the converter's code
